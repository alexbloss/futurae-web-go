package futuraeweb

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"encoding/hex"
	"errors"
	"strconv"
	"strings"
	"time"
)

const (
	ErrUSER    = "ERR|The username passed to sign_request() is invalid."
	ErrWID     = "ERR|The Futurae Web ID passed to sign_request() is invalid."
	ErrWKEY    = "ERR|The Futurae Web Key passed to sign_request() is invalid."
	ErrSKEY    = "ERR|The application Secret Key passed to sign_request() must be at least 40 characters."
	ErrUnknown = "ERR|An unknown error has occurred."

	sigSeparator = ":"
	valSeparator = "|"

	futuraePrefix       = "REQ"
	appPrefix           = "APP"
	authPrefix          = "AUTH"
	enrollPrefix        = "ENROLL"
	enrollRequestPrefix = "ENROLL_REQUEST"

	futuraeExpiry = 600
	appExpiry     = 3600

	widLen  = 36
	wkeyLen = 40
	skeyLen = 40
)

var (
	timeNow = time.Now
)

func SignRequest(wid, wkey, skey, username string) (string, error) {
	return signRequest(wid, wkey, skey, futuraePrefix, username)
}

func SignEnrollRequest(wid, wkey, skey, username string) (string, error) {
	return signRequest(wid, wkey, skey, enrollRequestPrefix, username)
}

func VerifyResponse(wid, wkey, skey, response string) string {
	return verifyResponse(wid, wkey, skey, response, authPrefix)
}

func VerifyEnrollResponse(wid, wkey, skey, response string) string {
	return verifyResponse(wid, wkey, skey, response, enrollPrefix)
}

func signRequest(wid, wkey, skey, prefix, username string) (string, error) {
	if len(username) == 0 || strings.Contains(username, valSeparator) {
		return "", errors.New(ErrUSER)
	}
	if wid == "" || len(wid) != widLen {
		return "", errors.New(ErrWID)
	}
	if wkey == "" || len(wkey) < wkeyLen {
		return "", errors.New(ErrWKEY)
	}
	if skey == "" || len(skey) < skeyLen {
		return "", errors.New(ErrSKEY)
	}

	futuraeSig := signVals(wkey, username, wid, prefix, futuraeExpiry)
	appSig := signVals(skey, username, wid, appPrefix, appExpiry)

	return strings.Join([]string{futuraeSig, appSig}, sigSeparator), nil
}

func signVals(key, username, wid, prefix string, expiry int64) string {
	exp := strconv.FormatInt(timeNow().Unix()+expiry, 10)

	val := strings.Join([]string{wid, username, exp}, valSeparator)
	b64 := base64.StdEncoding.EncodeToString([]byte(val))

	cookie := strings.Join([]string{prefix, b64}, valSeparator)
	sig := hmacSHA256(cookie, key)

	return strings.Join([]string{cookie, sig}, valSeparator)
}

func verifyResponse(wid, wkey, skey, response, prefix string) string {
	sigs := strings.Split(response, sigSeparator)
	if len(sigs) != 2 {
		return ""
	}

	authSig, appSig := sigs[0], sigs[1]

	user := parseVals(wkey, authSig, prefix, wid)
	appUser := parseVals(skey, appSig, appPrefix, wid)

	if user != appUser {
		return ""
	}

	return user
}

func parseVals(key, val, prefix, wid string) string {
	ts := timeNow().Unix()

	parts := strings.Split(val, valSeparator)
	if len(parts) != 3 {
		return ""
	}

	uprefix, ub64, usig := parts[0], parts[1], parts[2]

	cookie := strings.Join([]string{uprefix, ub64}, valSeparator)
	sig := hmacSHA256(cookie, key)

	if !hmac.Equal([]byte(sig), []byte(usig)) {
		return ""
	}

	if prefix != uprefix {
		return ""
	}

	decoded, err := base64.StdEncoding.DecodeString(ub64)
	if err != nil {
		return ""
	}

	cookieParts := strings.Split(string(decoded), valSeparator)
	if len(cookieParts) != 3 {
		return ""
	}

	uwid, username, expiry := cookieParts[0], cookieParts[1], cookieParts[2]

	if uwid != wid {
		return ""
	}

	expired, err := strconv.ParseInt(expiry, 10, 64)
	if err != nil {
		return ""
	}

	if ts >= expired {
		return ""
	}

	return username
}

func hmacSHA256(value, key string) string {
	h := hmac.New(sha256.New, []byte(key))
	h.Write([]byte(value))
	return hex.EncodeToString(h.Sum(nil))
}
